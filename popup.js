var popup = {
  exchanges: {
    'Agregado': 'Agregado de todas as exchanges',
    'ARN': 'Arena Bitcoin',
    'B2U': 'BitcoinToYou',
    'BAS': 'Basebit',
    'BIV': 'Bitinvest',
    'FLW': 'flowBTC',
    'FOX': 'FoxBit',
    'LOC': 'LocalBitcoins',
    'MBT': 'Mercado Bitcoin',
    'NEG': 'Negocie Coins'
  },

  setup: function() {
    $('#tabs').tabs();
    $(document).tooltip();
    this.initialRender();
    this.listenToStorage();
  },

  formatNumber: function(n, dp) {
    if (typeof dp === 'undefined') dp = 2;
    var w = n.toFixed(dp), k = w|0, b = n < 0 ? 1 : 0,
    u = Math.abs(w-k), d = (''+u.toFixed(dp)).substr(2, dp),
    s = ''+k, i = s.length, r = '';
    while ( (i-=3) > b ) { r = '.' + s.substr(i, 3) + r; }
    return s.substr(0, i + 3) + r + (d ? ','+d: '');
  },

  initialRender: function() {
    var _this = this;

    chrome.storage.local.get('tickerResponse', function(data) {
      if (typeof data.tickerResponse !== 'undefined') {
        _this.updateTables(data.tickerResponse);
      }
    });
  },

  listenToStorage: function() {
    var _this = this;

    chrome.storage.onChanged.addListener(function(changes) {
      if (typeof changes.tickerResponse !== 'undefined') {
        _this.updateTables(
          changes.tickerResponse.newValue,
          changes.tickerResponse.oldValue
        );
      }
    });
  },

  updateTables: function(newData, oldData) {
    this.updateTable('ticker_24h', newData.ticker_24h);
    this.updateTable('ticker_12h', newData.ticker_12h);
    this.updateTable('ticker_1h', newData.ticker_1h);
  },

  updateTable: function(id, newData, oldData) {
    var _this = this;
    var $tbody = $('#' + id + ' > tbody');

    $tbody.html('');

    this.appendRow($tbody, 'Agregado', newData.total);

    $.each(newData.exchanges, function(label, data) {
      _this.appendRow($tbody, label, data);
    });
  },

  appendRow: function(el, label, newData, oldData) {
    var _this = this;

    el.append(" \
      <tr> \
        <td title=\"" + _this.exchanges[label] + "\">" + label + "</td> \
        <td>" + _this.formatNumber(newData.last) + "</td> \
        <td>" + _this.formatNumber(newData.high) + "</td> \
        <td>" + _this.formatNumber(newData.low) + "</td> \
        <td>" + _this.formatNumber(newData.vol) + "</td> \
        <td>" + _this.formatNumber(newData.money) + "</td> \
        <td>" + _this.formatNumber(newData.vwap) + "</td> \
        <td>" + _this.formatNumber(newData.trades, 0) + "</td> \
      </tr> \
    ");
  },

  classForValue: function(oldValue, newValue) {
    return newValue > oldValue ? 'up' : 'down';
  }
};

document.addEventListener('DOMContentLoaded', function() {
  popup.setup();
});
