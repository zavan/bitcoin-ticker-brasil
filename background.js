var bg = {
  setup: function() {
    this.updateData();
    this.listenToStorage();
    this.createAlarm();
    this.listenToAlarm();
  },

  updateData: function() {
    $.getJSON('http://api.bitvalor.com/v1/ticker.json', function(response) {
      response.fetchedAt = Date.now();
      chrome.storage.local.set({ 'tickerResponse': response });
    });
  },

  listenToStorage: function() {
    var _this = this;

    chrome.storage.onChanged.addListener(function(changes) {
      if (typeof changes.tickerResponse !== 'undefined') {
        var tr = changes.tickerResponse;
        var oldValue = (tr.oldValue === 'undefined') ? 0 : tr.oldValue.ticker_24h.total.last;
        var newValue = changes.tickerResponse.newValue.ticker_24h.total.last;

        _this.updateBadge(oldValue, newValue);
      }
    });
  },

  createAlarm: function() {
    chrome.alarms.create('ticker', { delayInMinutes: 0, periodInMinutes: 1 });
  },

  listenToAlarm: function() {
    var _this = this;

    chrome.alarms.onAlarm.addListener(function(alarm) {
      if (alarm.name === 'ticker') _this.updateData();
    });
  },

  updateBadge: function(oldValue, newValue) {
    if (oldValue !== newValue) {
      var color = newValue > oldValue ? [51, 204, 51, 255] : [204, 0, 0, 255];

      chrome.browserAction.setBadgeBackgroundColor({ color: color });
      chrome.browserAction.setBadgeText({ text: newValue.toFixed(0).toString() });
    }
  }
};

bg.setup();
